const path = require('path');

module.exports = (eZConfig, eZConfigManager) => {
    eZConfigManager.replace({
        eZConfig,
        entryName: 'ezplatform-admin-ui-alloyeditor-js',
        itemToReplace: path.resolve(__dirname, '../../../../../vendor/ezsystems/ezplatform-admin-ui/src/bundle/Resources/public/js/alloyeditor/src/buttons/ez-btn-customtag-update.js'),
        newItem: path.resolve(__dirname, '../public/js/alloyeditor/buttons/ez-btn-customtag-update.js'),
    });
    eZConfigManager.replace({
        eZConfig,
        entryName: 'ezplatform-admin-ui-alloyeditor-js',
        itemToReplace: path.resolve(__dirname, '../../../../../vendor/ezsystems/ezplatform-admin-ui/src/bundle/Resources/public/js/alloyeditor/src/buttons/ez-btn-customtag-edit.js'),
        newItem: path.resolve(__dirname, '../public/js/alloyeditor/buttons/ez-btn-customtag-edit.js'),
    });
};
