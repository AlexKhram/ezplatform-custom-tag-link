import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AlloyEditor from 'alloyeditor';
import BaseEzBtnCustomTagEdit from '../../../../../../../../vendor/ezsystems/ezplatform-admin-ui/src/bundle/Resources/public/js/alloyeditor/src/buttons/ez-btn-customtag-edit';

export default class EzBtnCustomTagEdit extends BaseEzBtnCustomTagEdit {
    render() {
        if (this.props.renderExclusive || (this.getWidget() && this.getWidget().element.hasAttribute('data-show-edit-toolbar'))) {
            const buttonName = this.getUpdateBtnName();
            const ButtonComponent = AlloyEditor[buttonName];

            return <ButtonComponent values={this.getValues()} {...this.props} />;
        }

        const css = `ae-button ez-btn-ae ez-btn-ae--${this.customTagName}-edit`;

        return (
            <button className={css} onClick={this.props.requestExclusive} tabIndex={this.props.tabIndex}>
                <svg className="ez-icon ez-btn-ae__icon">
                    <use xlinkHref="/bundles/ezplatformadminui/img/ez-icons.svg#edit" />
                </svg>
            </button>
        );
    }
}

eZ.addConfig('ezAlloyEditor.ezBtnCustomTagEdit', EzBtnCustomTagEdit);

EzBtnCustomTagEdit.propTypes = {
    editor: PropTypes.object.isRequired,
    label: PropTypes.string.isRequired,
    tabIndex: PropTypes.number.isRequired,
};
