import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AlloyEditor from 'alloyeditor';
import BaseEzBtnCustomTagUpdate from '../../../../../../../../vendor/ezsystems/ezplatform-admin-ui/src/bundle/Resources/public/js/alloyeditor/src/buttons/ez-btn-customtag-update';

export default class EzBtnCustomTagUpdate extends BaseEzBtnCustomTagUpdate {
    componentWillUnmount() {
        ReactDOM.findDOMNode(this).parentNode.classList.remove('container-edit-mode');
        const widget = this.getWidget() || this.widget;
        if (this.state.discoveringContent || !widget) {
            return;
        }
        if (widget.element.hasAttribute('data-show-edit-toolbar')) {
            widget.element.removeAttribute('data-show-edit-toolbar')
        }
        if (widget.element.hasAttribute('data-ez-temporary-custom-tag')) {
            this.props.editor.get('nativeEditor').unlockSelection(true);
            widget.element.removeStyle('height');
            widget.element.removeStyle('visibility');
            widget.focus();

            this.props.editor.get('nativeEditor').execCommand('eZRemoveBlock', this.props.editor);
        }
    }

    componentDidMount() {
        ReactDOM.findDOMNode(this).parentNode.classList.add('container-edit-mode');
        if (!Object.keys(this.attributes).length) {
            this.saveCustomTag();
        }
    }

    selectContent(attrName, e) {
        const openUDW = () => {
            const udwContainer = document.querySelector('#react-udw');
            const token = document.querySelector('meta[name="CSRF-Token"]').content;
            const siteaccess = document.querySelector('meta[name="SiteAccess"]').content;
            const config = JSON.parse(document.querySelector(`[data-udw-config-name="richtext_embed"]`).dataset.udwConfig);
            const title = Translator.trans(/*@Desc("Select content")*/ 'link_edit_btn.udw.title', {}, 'alloy_editor');
            const alloyEditorCallbacks = eZ.ezAlloyEditor.callbacks;
            const mergedConfig = Object.assign(
                {
                    onConfirm: this.udwOnConfirm.bind(this, udwContainer, attrName),
                    onCancel: this.udwOnCancel.bind(this, udwContainer, attrName),
                    title,
                    multiple: false,
                    startingLocationId: window.eZ.adminUiConfig.universalDiscoveryWidget.startingLocationId,
                    restInfo: {token, siteaccess},
                },
                config
            );

            if (alloyEditorCallbacks && typeof alloyEditorCallbacks.openUdw === 'function') {
                alloyEditorCallbacks.openUdw(mergedConfig);
            } else {
                ReactDOM.render(React.createElement(eZ.modules.UniversalDiscovery, mergedConfig), udwContainer);
            }
        };

        this.setState(
            {
                discoveringContent: true,
            },
            openUDW.bind(this)
        );
    }

    udwOnConfirm(udwContainer, attrName, items) {
        const values = Object.assign({}, this.state.values);
        values[attrName].value = 'eznode://' + items[0].id;

        this.setState({
            values: values,
        });

        this.udwOnCancel(udwContainer);
    }

    udwOnCancel(udwContainer) {
        const {createNewTag, editor} = this.props;
        if (createNewTag) {
            editor.get('nativeEditor').unlockSelection(true);
            this.execCommand();
        }
        const widget = this.getWidget() || this.widget;
        if (!widget) {
            return;
        }
        if (createNewTag) {
            widget.element.setAttribute('data-ez-temporary-custom-tag', true);
            widget.element.setStyles({height: 0, visibility: 'hidden'});
        }

        const configValues = Object.assign({}, this.state.values);
        widget.setName(this.customTagName);
        widget.setWidgetAttributes(this.createAttributes());
        widget.renderHeader();
        widget.clearConfig();

        Object.keys(this.attributes).forEach((key) => {
            widget.setConfig(key, configValues[key].value);
        });
        widget.element.setAttribute('data-show-edit-toolbar', true);

        ReactDOM.unmountComponentAtNode(udwContainer);
    }

    clearLink(attrName) {
        const values = Object.assign({}, this.state.values);
        values[attrName].value = '';

        this.setState({
            values: values,
        });
    }

    renderLink(config, attrName) {
        const selectContentLabel = Translator.trans(
            /*@Desc("Select content")*/ 'link_edit_btn.button_row.select_content',
            {},
            'alloy_editor'
        );
        const separatorLabel = Translator.trans(/*@Desc("or")*/ 'link_edit_btn.button_row.separator', {}, 'alloy_editor');
        const linkToLabel = Translator.trans(/*@Desc("Link to:")*/ 'link_edit_btn.button_row.link_to', {}, 'alloy_editor');
        const selectLabel = Translator.trans(/*@Desc("Select:")*/ 'link_edit_btn.button_row.select', {}, 'alloy_editor');
        const blockPlaceholderText = Translator.trans(
            /*@Desc("Type or paste link here")*/ 'link_edit_btn.button_row.block.placeholder.text',
            {},
            'alloy_editor'
        );
        const containerClasses = 'ez-ae-edit-link__row ez-ae-edit-link__row--udw ez-ae-edit-link' + (this.state.values[attrName].value ? ' is-linked' : '');

        return (
            <div className={containerClasses}>
                <div className="ez-ae-edit-link__block ez-ae-edit-link__block--udw">
                    <label className="ez-ae-edit-link__label">{selectLabel}</label>
                    <button className="ez-ae-button ez-btn-ae ez-btn-ae--udw btn btn-secondary"
                            onClick={this.selectContent.bind(this, attrName)}>
                        {selectContentLabel}
                    </button>
                </div>
                <div className="ez-ae-edit-link__block ez-ae-edit-link__block--separator">
                    <span className="ez-ae-edit-link__text">{separatorLabel}</span>
                </div>
                <div className="ez-ae-edit-link__block ez-ae-edit-link__block--url">
                    <label className="ez-ae-edit-link__label">{linkToLabel}</label>
                    <input
                        placeholder={blockPlaceholderText}
                        type="text"
                        defaultValue={config.defaultValue}
                        required={config.required}
                        className="attribute__input form-control ae-input ez-ae-edit-link__input"
                        value={this.state.values[attrName].value}
                        onChange={this.updateValues.bind(this)}
                        data-attr-name={attrName}
                    />
                    <button
                        aria-label={AlloyEditor.Strings.clearInput}
                        className="ez-btn-ae ez-btn-ae--clear-link ae-button ae-icon-remove"
                        onClick={this.clearLink.bind(this, attrName)}
                        title={AlloyEditor.Strings.clear}
                    />
                </div>
            </div>
        );
    }

    saveCustomTag() {
        const {createNewTag, editor} = this.props;

        editor.get('nativeEditor').unlockSelection(true);

        if (createNewTag) {
            this.execCommand();
        }

        const widget = this.getWidget() || this.widget;
        if (widget.element.hasAttribute('data-ez-temporary-custom-tag')) {
            widget.element.removeAttribute('data-ez-temporary-custom-tag');
            widget.element.removeStyle('height');
            widget.element.removeStyle('visibility');
        }
        const configValues = Object.assign({}, this.state.values);

        widget.setFocused(true);
        widget.setName(this.customTagName);
        widget.setWidgetAttributes(this.createAttributes());
        widget.renderHeader();
        widget.clearConfig();

        Object.keys(this.attributes).forEach((key) => {
            widget.setConfig(key, configValues[key].value);
        });
    }

    getAttributeRenderMethods() {
        return {
            string: 'renderString',
            boolean: 'renderCheckbox',
            number: 'renderNumber',
            choice: 'renderSelect',
            link: 'renderLink',
        };
    }
}

eZ.addConfig('ezAlloyEditor.ezBtnCustomTagUpdate', EzBtnCustomTagUpdate);

EzBtnCustomTagUpdate.defaultProps = {
    command: 'ezcustomtag',
    modifiesSelection: true,
};

EzBtnCustomTagUpdate.propTypes = {
    editor: PropTypes.object.isRequired,
    label: PropTypes.string.isRequired,
    tabIndex: PropTypes.number.isRequired,
};
